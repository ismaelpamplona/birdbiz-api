const express = require("express");
const router = express.Router();
const companyController = require("../controllers/companyController");

router.get("/", companyController.getAllCompanies);

router.post(
    "/",
    companyController.upload.single("companyLogo"),
    companyController.createCompany
);

router.get("/:companyId", companyController.getCompany);

router.patch("/:companyId", companyController.updateCompany);

router.delete("/:companyId", companyController.deleteCompany);

module.exports = router;
