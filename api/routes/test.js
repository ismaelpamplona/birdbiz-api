const express = require("express");
const router = express.Router();

// Just some examples to practice

const myMiddleware = (req, res, next) => {
    req.pussyMid = "This STRING Data is coming from my middleware =D";
    next();
};

const homepage = (req, res) => {
    const isma = {
        msg: "birdbiz management system API!",
        mid: req.pussyMid,
        author: {
            name: "Ismael Jedi Pamplona 👊👊👊",
            age: 33,
            cool: true
        }
    };
    res.json(isma);
};

const queryTest = (req, res) => {
    // http://localhost:9999/test/query/?name=Ismael&age=33&cool=true
    res.json(req.query);
};

const reverse = (req, res) => {
    // http://localhost:9999/test/reverse/racecar
    const reverse = [...req.params.name].reverse().join("");
    res.send(reverse);
};

// Just some examples
router.get("/", myMiddleware, homepage);
router.get("/query", queryTest);
router.get("/reverse/:name", reverse);

module.exports = router;
