const express = require("express");
const router = express.Router();

const checkAuth = require("../middleware/checkAuth");
const userValidation = require("../middleware/userValidation");
const userUpdateValidation = require("../middleware/userUpdateValidation");
const sendMail = require("../middleware/mail");
const userController = require("../controllers/userController");

router.post("/isLoggedIn", checkAuth, userController.isLoggedIn);

router.patch(
    "/update",
    userController.login,
    userUpdateValidation,
    userController.updateUser
);

router.post("/singup", userValidation, userController.createUser);

router.post("/login", userController.login);

router.post("/forgot", userController.forgot, sendMail);

router.post("/reset/", userController.reset, userController.updateUser);

router.delete("/:userId", checkAuth, userController.deleteUser);

module.exports = router;
