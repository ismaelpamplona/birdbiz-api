const mongoose = require("mongoose");

const companySchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        trim: true,
        required: "Please enter a company name!"
    },
    description: {
        type: String,
        trim: true,
        required: "Please enter a company description!"
    },
    companyImage: { type: String, required: true }
});

module.exports = mongoose.model("Company", companySchema);
