const mongoose = require("mongoose");
const validator = require("validator");

const userSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: {
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        validate: [validator.isEmail, "Invalid e-mail address!"],
        required: "Please supply an e-mail address!"
    },
    password: { type: String, required: "Please supply a password!" },
    resetPasswordToken: String,
    resetPasswordExpires: Date
});

module.exports = mongoose.model("User", userSchema);
