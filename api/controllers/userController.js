const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const crypto = require("crypto"); // node native package

const User = require("../models/user");

exports.createUser = (req, res, next) => {
    bcrypt.hash(req.body.password, 10, (err, hash) => {
        if (err) {
            return res.status(500).json({ error: { message: err } });
        } else {
            const user = new User({
                _id: new mongoose.Types.ObjectId(),
                email: req.body.email,
                password: hash
            });
            user.save()
                .then(result => {
                    const token = jwt.sign(
                        {
                            email: result.email,
                            userId: result._id
                        },
                        process.env.JWT_KEY,
                        { expiresIn: "1h" }
                    );
                    res.status(200).json({
                        result: result,
                        message: "User created!",
                        token: token
                    });
                })
                .catch(err => {
                    res.status(500).json({ error: err });
                });
        }
    });
};

exports.login = (req, res, next) => {
    User.find({ email: req.body.email })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({ message: "Auth failed! 🤕" });
            }
            bcrypt.compare(
                req.body.password,
                user[0].password,
                (err, result) => {
                    if (err) {
                        return res
                            .status(401)
                            .json({ message: "Auth failed! 🤕" });
                    }
                    if (result) {
                        const token = jwt.sign(
                            { email: user[0].email, userId: user[0]._id },
                            process.env.JWT_KEY,
                            { expiresIn: "1h" }
                        );
                        return res.status(200).json({
                            message: "Auth successful!",
                            token: token
                        });
                    }
                    res.status(401).json({ message: "Auth failed! 🤕" });
                }
            );
        })
        .catch(err => {
            res.status(500).json({ error: err });
        });
};

exports.isLoggedIn = (req, res, next) => {
    res.status(200).json({ isLoggedIn: true });
};

exports.updateUser = (req, res, next) => {
    const password = req.body.newpassword
        ? req.body.newpassword
        : req.body.password;

    User.findOne({ email: req.body.email })
        .exec()
        .then(user => {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({ error: { message: err } });
                } else {
                    const id = user._id;
                    const updatedUser = {
                        email: req.body.newemail
                            ? req.body.newemail
                            : user.email,
                        password: hash
                    };
                    User.update({ _id: id }, { $set: updatedUser })
                        .exec()
                        .then(result => {
                            res.status(200).json({
                                result: result,
                                message: "User updated!"
                            });
                        })
                        .catch(err => {
                            res.status(500).json({ error: err });
                        });
                }
            });
        })
        .catch(err => {
            res.status(500).json({ error: err });
        });
};

exports.deleteUser = (req, res, next) => {
    User.remove({ _id: req.params.userId })
        .exec()
        .then(result => {
            res.status(200).json({ message: "User deleted!" });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};

exports.forgot = (req, res, next) => {
    if (req.body.email === "") {
        return res.status(409).json({
            error: {
                message:
                    "🤔 Please! Help me helping you! Type the fucking data! 😤"
            }
        });
    }
    User.findOne({ email: req.body.email })
        .exec()
        .then(user => {
            if (!user) {
                return res.status(200).json({
                    message: "A password reset has been mailed to you! 🤓"
                }); // fake message
            } else {
                user.resetPasswordToken = crypto
                    .randomBytes(20)
                    .toString("hex");
                user.resetPasswordExpires = Date.now() + 300000000; // 5 minutes from now TAKE OFF 3 ZEROS !!!
                user.save();
                req.token = user.resetPasswordToken;
                next();
            }
        })
        .catch(err => {
            return res.status(500).json({ error: "piroca" });
        });
};

exports.reset = (req, res, next) => {
    if (
        req.body.email === "" ||
        req.body.password === "" ||
        req.body.passwordConfirmation === ""
    ) {
        return res.status(409).json({
            error: {
                message:
                    "🤔 Please! Help me helping you! Type the fucking data! 😤"
            }
        });
    }
    if (!req.body.resetPasswordToken) {
        return res.status(409).json({
            error: { message: "Are you trying to cheat me? 🧐" }
        });
    }
    User.findOne({
        email: req.body.email,
        resetPasswordToken: req.body.resetPasswordToken,
        resetPasswordExpires: { $gt: Date.now() }
    })
        .exec()
        .then(user => {
            if (!user) {
                return res.status(409).json({
                    error: {
                        message: "Password reset is invalid or expired! 🤕"
                    }
                });
            } else if (req.body.password !== req.body.passwordConfirmation) {
                return res.status(409).json({
                    error: { message: "Oops! Your passwords do not match!" }
                });
            } else {
                next();
            }
        })
        .catch(err => {
            res.status(500).json({ error: err });
        });
};
