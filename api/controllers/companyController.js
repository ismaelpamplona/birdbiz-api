const multer = require("multer");
const mongoose = require("mongoose");
const Company = require("../models/company");

const api_url = process.env.API_URL + "/companies/";

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, "./uploads/");
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + "-" + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    cb(null, false);
    if (
        file.mimetype === "image/jpeg" ||
        file.mimetype === "image/png" ||
        file.mimetype === "image/gif"
    ) {
        cb(null, true); // accept a file
    } else {
        cb(null, fase); // reject a file
    }
};

exports.upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 * 5 },
    fileFilter: fileFilter
});

exports.getAllCompanies = (req, res, next) => {
    Company.find()
        .select("_id name description")
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                companies: docs.map(doc => {
                    console.log(doc);
                    return {
                        _id: doc._id,
                        name: doc.name,
                        description: doc.description,
                        companyImage: doc.companyImage,
                        request: {
                            type: "GET",
                            url: api_url + doc._id
                        }
                    };
                })
            };
            // if (docs.length >= 0) {
            res.status(200).json(response);
            // } else {
            //     res.status(404).json({
            //         error: "No entries found!"
            //     });
            // }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.createCompany = (req, res, next) => {
    console.log(req.file);
    const company = new Company({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        description: req.body.description,
        companyImage: req.file.path
    });
    company
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: "Created company succesfully",
                createdCompany: {
                    _id: result._id,
                    name: result.name,
                    description: result.description,
                    resquest: {
                        type: "GET",
                        url: api_url
                    }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ err: err });
        });
};

exports.getCompany = (req, res, next) => {
    const id = req.params.companyId;
    Company.findById(id)
        .select("_id name description companyImage")
        .exec()
        .then(doc => {
            if (doc) {
                res.status(200).json({
                    company: doc,
                    request: {
                        type: "GET",
                        url: api_url + doc._id
                    }
                });
            } else {
                res.status(404).json({
                    message: "No valid entry found for provided ID!"
                });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};

exports.updateCompany = (req, res, next) => {
    const id = req.params.companyId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    Company.update({ _id: id }, { $set: updateOps })
        .exec()
        .then(result => {
            console.log(result);
            res.status(200).json({
                result: result,
                message: "Company updated!",
                request: {
                    typoe: "GET",
                    url: api_url + id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};

exports.deleteCompany = (req, res, next) => {
    const id = req.params.companyId;
    Company.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                result: result,
                message: "Company deleted!",
                resquest: {
                    type: "POST",
                    url: api_url,
                    body: { name: "String", description: "String" }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};
