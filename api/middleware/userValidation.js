const User = require("../models/user");

module.exports = (req, res, next) => {
    if (
        req.body.email === "" ||
        req.body.password === "" ||
        req.body.passwordConfirmation === ""
    ) {
        return res.status(409).json({
            error: {
                message:
                    "🤔 Please! Help me helping you! Type the fucking data! 😤"
            }
        });
    }
    User.findOne({ email: req.body.email })
        .exec()
        .then(user => {
            console.log(user);
            if (user) {
                return res
                    .status(409)
                    .json({ error: { message: "User exists" } });
            } else if (req.body.password !== req.body.passwordConfirmation) {
                return res.status(409).json({
                    error: { message: "Oops! Your passwords do not match!" }
                });
            }
            next();
        });
};
