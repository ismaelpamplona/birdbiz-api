const User = require("../models/user");

module.exports = (req, res, next) => {
    if (req.body.email === "") {
        return res
            .status(409)
            .json({ error: { message: "Somthing got wrong! 🤕" } });
    }
    User.findOne({ email: req.body.newemail })
        .exec()
        .then(user => {
            if (user && user.email !== req.body.email) {
                return res.status(409).json({
                    error: { message: "This email is already registered!" }
                });
            }
        });
    if (
        (req.body.newpassword || req.body.passwordConfirmation) &&
        req.body.newpassword !== req.body.passwordConfirmation
    ) {
        return res.status(409).json({
            error: { message: "Oops! Your passwords do not match!" }
        });
    }
    next();
};
