const nodemailer = require("nodemailer");
const smtpTransport = require("nodemailer-smtp-transport");
const generateHtml = require("./email-template/renderHtmlMailResetFlow");
const htmlToText = require("html-to-text");

module.exports = (req, res, next) => {
    const html = generateHtml(
        `https://ismaelpamplona.gitlab.io/birdbiz/#/reset?token=${req.token}`
    );
    const text = htmlToText.fromString(html);
    const mailOptions = {
        from: `Birdbiz <${process.env.MAIL_USER}>`,
        to: req.body.email,
        subject: "Birdbiz Password Reset",
        html,
        text
    };

    const transporter = nodemailer.createTransport(
        smtpTransport({
            service: process.env.MAIL_SERVICE,
            host: process.env.MAIL_HOST,
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASS
            }
        })
    );

    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            return res
                .status(409)
                .json({ error: { message: "Somthing got wrong! 🤕" } });
        } else {
            return res.status(200).json({
                message: "A password reset has been mailed to you! 🤓"
            }); // true message
        }
    });
};
