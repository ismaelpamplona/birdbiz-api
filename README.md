<h1><img height="80px" src="https://birdbiz-api.herokuapp.com/uploads/birdbiz-backend-favicon.png"> <b>birdbiz</b> management system backend</h1>

<h3>Try in <a  href="https://birdbiz-api.herokuapp.com/test">https://birdbiz-api.herokuapp.com/test</a></h3>

## Table of Contents

1. [Install](#install)
2. [Stack Used](#stack-used)

<h2 align="center">Install</h2>

```bash
npm install
```

Then run:

```bash
npm start
```

<h2 align="center">Stack Used</h2>

<!-- | <a href=""></a> | | | -->

| Name                                                                                                                                                          | Link                                                                                                  | Description                                                                                                                                                                                            |
| :------------------------------------------------------------------------------------------------------------------------------------------------------------ | :---------------------------------------------------------------------------------------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| <a href="https://nodejs.org/"><img height="30px" src="https://nodejs.org/static/images/logo.svg"></a>                                                         | https://nodejs.org/                                                                                   | Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.                                                                                                                               |
| <a href="https://nodemon.io/"><img height="30px" src="https://user-images.githubusercontent.com/13700/35731649-652807e8-080e-11e8-88fd-1b2f6d553b2d.png"></a> | https://nodemon.io/                                                                                   | Nodemon is a utility that will monitor for any changes in your source and automatically restart your server                                                                                            |
| <a href="https://expressjs.com/">Express</a>                                                                                                                  | https://expressjs.com/                                                                                | Fast, unopinionated, minimalist web framework for Node.js                                                                                                                                              |
| <a href="https://mongoosejs.com/"><img height="30px" src="https://res.cloudinary.com/dxwefujke/image/upload/v1528398729/20160620102203_885_o1fgwu.jpg"></a>   | https://github.com/Automattic/mongoose                                                                | Elegant mongodb object modeling for node.js                                                                                                                                                            |
| <a href="https://github.com/expressjs/morgan/">Morgan</a>                                                                                                     | https://github.com/expressjs/morgan                                                                   | HTTP request logger middleware for node.js                                                                                                                                                             |
| <a href="https://github.com/motdotla/dotenv">dotenv</a>                                                                                                       | https://github.com/motdotla/dotenv                                                                    | Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env                                                                                                  |
| <a href="https://github.com/expressjs/body-parser">body-parser</a>                                                                                            | https://github.com/expressjs/body-parser                                                              | Node.js body parsing middleware - Parse incoming request bodies in a middleware before your handlers, available under the req.body property                                                            |
| <a href="https://github.com/expressjs/multer">Multer</a>                                                                                                      | https://github.com/expressjs/multer                                                                   | Multer is a node.js middleware for handling multipart/form-data, which is primarily used for uploading files.                                                                                          |
| <a href="https://github.com/chriso/validator.js">Validator</a>                                                                                                | https://github.com/chriso/validator.js                                                                | A library of string validators and sanitizers.                                                                                                                                                         |
| <a href="https://github.com/kelektiv/node.bcrypt.js">node.bcrypt.js</a>                                                                                       | https://github.com/kelektiv/node.bcrypt.js                                                            | A library to help you hash passwords.                                                                                                                                                                  |
| <a href="https://github.com/auth0/node-jsonwebtoken"><img height="20px" src="https://jwt.io/img/logo.svg"></a>                                                | https://github.com/auth0/node-jsonwebtoken                                                            | An implementation of <a href="https://tools.ietf.org/html/rfc7519">JSON Web Tokens</a>                                                                                                                 |
| <a href="https://github.com/werk85/node-html-to-text">html-to-text</a>                                                                                        | <a href="https://github.com/werk85/node-html-to-text">https://github.com/werk85/node-html-to-text</a> | An advanced converter that parses HTML and returns beautiful text. It was mainly designed to transform HTML E-Mail templates to a text representation. So it is currently optimized for table layouts. |
