const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const checkAuth = require("./api/middleware/checkAuth");

const testRoutes = require("./api/routes/test");
const companiesRoutes = require("./api/routes/companies");
const userRoutes = require("./api/routes/user");

app.use(morgan("dev"));
app.use("/uploads", express.static("uploads"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Set the permissions for a CORS - Cross-Origin Resource Sharing mechanism
app.use((req, res, next) => {
    // res.header("Access-Control-Allow-Origin", process.env.CORS_ORIGIN_ACCESS);
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
        res.header(
            "Access-Control-Allow-Methods",
            "PUT, POST, PATCH, DELETE, GET"
        );
        return res.status(200).json({});
    }
    next();
});

// Routes which should handle requests
app.use("/test", testRoutes);
app.use("/companies", checkAuth, companiesRoutes);
app.use("/user", userRoutes);

// Handle every request that reaches this line - no route above was able to handle the request
app.use((req, res, next) => {
    const error = new Error("Not found!");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;
