const http = require("http");
const app = require("./app");
const mongoose = require("mongoose");

// import environmental variables from our variables.env file
require("dotenv").config({ path: "variables.env" });

// Connect to our Database and handle any bad connections
mongoose.connect("mongodb://admin:admin2019@ds239936.mlab.com:39936/birdbiz", {
    useNewUrlParser: true
});
mongoose.set("useCreateIndex", true);

// Tell Mongoose to use the default node.js promise implementation
mongoose.Promise = global.Promise;

mongoose.connection.on("error", err => {
    console.error(`💩 🚫 💩 🚫 💩 🚫 💩 🚫 → ${err.message}`);
});

const port = process.env.PORT || 8888;

const server = http.createServer(app);

server.listen(port);
